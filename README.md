# price_action_lib

## Description
price_action_lib is a Python library for detecting and analyzing price action patterns in financial markets. It provides an object-oriented approach to create custom trading strategies based on candlestick chart patterns and technical indicators.

## Installation
To install price_action_lib, first, clone the repository:

```git clone https://github.com/KeremSoke/price_action_lib.git```

Then, navigate to the project directory and run the setup script:
  
  ```cd price_action_lib```
  
  ```python setup.py install```

## Usage
The library provides two main modules: patterns and strategies.

### Patterns
The patterns module contains functions for identifying candlestick chart patterns, such as "Bullish Engulfing" or "Three Inside Up". Each function takes a Pandas DataFrame containing historical price data as input and returns a boolean Pandas Series indicating whether each row in the DataFrame is a pattern or not.

```
import price_action_lib.patterns as patterns
import pandas as pd

data = pd.read_csv('AAPL.csv')
bullish_engulfing = patterns.is_bullish_engulfing(data)
```

### Strategies
The strategies module contains classes for creating custom trading strategies based on candlestick chart patterns and technical indicators. Each class inherits from the PriceActionStrategy base class and implements the generate_signals method, which takes a Pandas DataFrame containing historical price data as input and returns a Pandas DataFrame containing trading signals.

```
import price_action_lib.strategies as strategies
import pandas as pd

data = pd.read_csv('AAPL.csv')
strategy = strategies.BullishEngulfingStrategy()
signals = strategy.generate_signals(data)
```

## Support
For further help, you can reach me from matrix. My matrix name: @pelo_the_cat:mozilla.org

## Contributing
Contributions to price_action_lib are welcome. If you find a bug or have an idea for a new feature, please open an issue or submit a pull request.

## License
price_action_lib is licensed under the GPL-3.0 License. For more information, see the LICENSE file and/or GPL site: https://www.gnu.org/licenses/gpl-3.0.en.html.
