import pandas as pd


class Backtester:
    def __init__(self, data, signals, initial_capital=1000000):
        self.data = data.copy()
        self.signals = signals
        self.initial_capital = initial_capital
        self.trade_log = pd.DataFrame(columns=['timestamp', 'symbol', 'signal', 'price', 'qty'])
        self.portfolio_log = pd.DataFrame(columns=['timestamp', 'capital', 'shares_held', 'position_value',
                                                   'total_value', 'cumulative_return'])

    def backtest(self, strategy_name, slippage=0.01, commission=0.01):
        """
        Backtest the given strategy using the provided signals and data.

        Args:
            strategy_name (str): The name of the strategy to backtest.
            slippage (float, optional): The percentage of slippage to simulate for each trade. Defaults to 0.01.
            commission (float, optional): The percentage commission to simulate for each trade. Defaults to 0.01.

        Returns:
            pd.DataFrame: A DataFrame containing the trade log and performance metrics.
        """
        # Get the signals for the specified strategy
        signals = self.signals[strategy_name]

        # Initialize the portfolio
        capital = self.initial_capital
        shares_held = 0
        position_value = 0
        total_value = capital
        cumulative_return = 0

        # Iterate over the signals and execute trades
        for i, signal in enumerate(signals):
            timestamp = self.data.index[i]
            price = self.data.loc[timestamp, 'Close']
            qty = 0

            # Check if we have a signal to buy
            if signal == 'buy':
                # Calculate the number of shares we can afford to buy
                qty = int((capital * (1 - commission)) / price)
                if qty > 0:
                    # Calculate the transaction cost
                    transaction_cost = qty * price * (1 + slippage)
                    # Subtract the transaction cost from our capital
                    capital -= transaction_cost
                    # Add the shares to our portfolio
                    shares_held += qty
                    # Update the position value and total value
                    position_value = shares_held * price
                    total_value = capital + position_value
                    # Log the trade
                    self.trade_log.loc[len(self.trade_log)] = [timestamp, 'buy', price, qty]

            # Check if we have a signal to sell
            elif signal == 'sell':
                # Sell all of our shares
                qty = shares_held
                if qty > 0:
                    # Calculate the transaction cost
                    transaction_cost = qty * price * (1 - slippage)
                    # Add the transaction cost to our capital
                    capital += transaction_cost
                    # Subtract the shares from our portfolio
                    shares_held -= qty
                    # Update the position value and total value
                    position_value = shares_held * price
                    total_value = capital + position_value
                    # Log the trade
                    self.trade_log.loc[len(self.trade_log)] = [timestamp, 'sell', price, qty]

            # Update the portfolio log
            self.portfolio_log.loc[len(self.portfolio_log)] = [timestamp, capital, shares_held, position_value,
                                                                total_value, cumulative_return]

        # Calculate performance metrics
        metrics = self._calculate_metrics()

        # Return the trade log and performance metrics
        return pd.concat([self.trade_log, metrics], axis=1)

    def _calculate_metrics(self):
        """
        Calculate performance metrics for the backtested strategy.
        """
        # Calculate cumulative returns
        self.portfolio['returns'] = self.portfolio['balance'].pct_change().fillna(0)
        self.portfolio['cum_returns'] = (1 + self.portfolio['returns']).cumprod()
        # Calculate performance metrics
        self.metrics['profit_factor'] = self._calculate_profit_factor()
        self.metrics['win_rate'] = self._calculate_win_rate()
        self.metrics['average_win'] = self._calculate_average_win()
        self.metrics['average_loss'] = self._calculate_average_loss()
        self.metrics['sharpe_ratio'] = self._calculate_sharpe_ratio()
        self.metrics['drawdown'] = self._calculate_drawdown()
        self.metrics['max_drawdown'] = self._calculate_max_drawdown()

    def _calculate_profit_factor(self):
        """
        Calculate the profit factor for the backtested strategy.
        """
        wins = self.trades['pnl'][self.trades['pnl'] > 0].sum()
        losses = abs(self.trades['pnl'][self.trades['pnl'] < 0].sum())
        return wins / losses

    def _calculate_win_rate(self):
        """
        Calculate the win rate for the backtested strategy.
        """
        num_wins = len(self.trades[self.trades['pnl'] > 0])
        num_losses = len(self.trades[self.trades['pnl'] < 0])
        total_trades = num_wins + num_losses
        return num_wins / total_trades

    def _calculate_average_win(self):
        """
        Calculate the average win for the backtested strategy.
        """
        return self.trades['pnl'][self.trades['pnl'] > 0].mean()

    def _calculate_average_loss(self):
        """
        Calculate the average loss for the backtested strategy.
        """
        return abs(self.trades['pnl'][self.trades['pnl'] < 0].mean())

    def _calculate_sharpe_ratio(self):
        """
        Calculate the Sharpe ratio for the backtested strategy.
        """
        returns = self.portfolio['returns']
        risk_free_rate = 0  # Assuming risk-free rate is 0 for simplicity
        sharpe_ratio = np.sqrt(len(returns)) * (returns.mean() - risk_free_rate) / returns.std()
        return sharpe_ratio

    def _calculate_drawdown(self):
        """
        Calculate the drawdown for the backtested strategy.
        """
        cumulative_returns = self.portfolio['cum_returns']
        rolling_max = cumulative_returns.rolling(window=len(cumulative_returns), min_periods=1).max()
        drawdown = cumulative_returns / rolling_max - 1.0
        return drawdown

    def _calculate_max_drawdown(self):
        """
        Calculate the maximum drawdown for the backtested strategy.
        """
        drawdown = self._calculate_drawdown()
        max_drawdown = abs(drawdown.min())
        return max_drawdown

class BacktestResult:
    def init(self, strategy_name, signals, data, initial_capital):
        self.strategy_name = strategy_name
        self.signals = signals
        self.data = data
        self.initial_capital = initial_capital
        self.positions = pd.DataFrame(index=data.index)
        self.trades = pd.DataFrame(index=data.index)
        self.performance = None

    def run_backtest(self, position_size=0.1, stop_loss_pct=0.05, take_profit_pct=0.1):
        """
        Simulate the backtest for the given strategy, with the given parameters.

        Args:
            position_size (float): The fraction of the portfolio to risk per trade.
            stop_loss_pct (float): The maximum percentage loss allowed per trade.
            take_profit_pct (float): The percentage profit target for each trade.

        Returns:
            pd.DataFrame: A DataFrame containing the trade signals and performance metrics.
        """
        # Initialize variables
        capital = self.initial_capital
        stop_loss = None
        take_profit = None
        self.positions['position_size'] = 0.0
        self.positions['position_value'] = 0.0
        self.positions['position_type'] = None
        self.trades['entry_price'] = 0.0
        self.trades['exit_price'] = 0.0
        self.trades['entry_time'] = None
        self.trades['exit_time'] = None
        self.trades['position_size'] = 0.0
        self.trades['profit_loss'] = 0.0

        # Loop through each day of data
        for i, row in self.signals.iterrows():
            if row[self.strategy_name] == 'buy':
                # Calculate the position size based on the risk parameters and available capital
                position_size = capital * position_size / (self.data.loc[i, 'Open'] * (1 + stop_loss_pct))
                position_size = min(position_size, capital)
                self.positions.loc[i:, 'position_size'] = position_size
                self.positions.loc[i:, 'position_value'] = position_size * self.data.loc[i, 'Open']
                self.positions.loc[i:, 'position_type'] = 'long'
                self.trades.loc[i, 'entry_price'] = self.data.loc[i, 'Open']
                self.trades.loc[i, 'entry_time'] = i
                self.trades.loc[i, 'position_size'] = position_size

                # Set the stop loss and take profit levels
                stop_loss = self.data.loc[i, 'Open'] * (1 - stop_loss_pct)
                take_profit = self.data.loc[i, 'Open'] * (1 + take_profit_pct)

            elif row[self.strategy_name] == 'sell':
                # Calculate the position size based on the risk parameters and available capital
                position_size = capital * position_size / (self.data.loc[i, 'Open'] * (1 - stop_loss_pct))
                position_size = min(position_size, capital)
                self.positions.loc[i:, 'position_size'] = -position_size
                self.positions.loc[i:, 'position_value'] = -position_size * self.data.loc[i, 'Open']
                self.positions.loc[i:, 'position_type'] = 'short'
                self.trades.loc[i, 'entry_price'] = self.data.loc[i, 'Open']
                self.trades.loc[i, 'entry_time'] = i
                self.trades.loc[i, 'position_size'] = -position_size

                # Set the stop loss and take profit levels
                stop_loss = self.data.loc[i, 'Open'] * (1 + stop_loss_pct)
                take_profit = self
    
    def backtest(strategy, initial_capital=10000, commission=0.001):
        """
        Backtest a strategy.
        
        Args:
            strategy (PriceActionStrategy): The strategy to backtest.
            initial_capital (float): The initial amount of capital to start trading with.
            commission (float): The commission rate for each trade, as a percentage of the trade value.

        Returns:
            pd.DataFrame: A DataFrame containing the trade history and performance metrics.
        """
        signals = strategy.signals.dropna()
        if signals.empty:
            raise ValueError("No signals generated for the strategy.")

        # Create a portfolio to keep track of trades
        portfolio = Portfolio(initial_capital, commission=commission)

        for date, signal in signals.iteritems():
            if signal == 'buy':
                portfolio.buy(date, strategy.data.loc[date, 'Close'])
            elif signal == 'sell':
                portfolio.sell(date, strategy.data.loc[date, 'Close'])

        return portfolio.get_trade_history()
