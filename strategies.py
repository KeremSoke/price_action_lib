#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  strategies.py
#  
#  Copyright 2023 Kerem Soke <ksoke@student.ubc.ca>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import pandas as pd
import numpy as np
import talib
from price_action_lib import patterns


class PriceActionStrategy:
    def __init__(self, data):
        self.data = data.copy()
        self._add_pattern_indicators()
        self._add_talib_indicators()
        self.signals = pd.DataFrame(index=data.index)
        self.scores = pd.DataFrame(index=data.index)

    def _add_pattern_indicators(self):
        # Add pattern indicators from price_action_lib.patterns module
        self.data['BullishEngulfing'] = patterns.is_bullish_engulfing(self.data)
        self.data['BearishEngulfing'] = patterns.is_bearish_engulfing(self.data)
        self.data['MorningStar'] = patterns.is_morning_star(self.data)
        self.data['EveningStar'] = patterns.is_evening_star(self.data)
        self.data['ThreeWhiteSoldiers'] = patterns.is_three_white_soldiers(self.data)
        self.data['ThreeBlackCrows'] = patterns.is_three_black_crows(self.data)
        self.data['PiercingLine'] = patterns.is_piercing_line(self.data)
        self.data['DarkCloudCover'] = patterns.is_dark_cloud_cover(self.data)
        self.data['ThreeInsideUp'] = patterns.is_three_inside_up(self.data)
        self.data['ThreeInsideDown'] = patterns.is_three_inside_down(self.data)

    def _add_talib_indicators(self):
        # Add talib indicators for more advanced strategies
        close = self.data['Close']
        high = self.data['High']
        low = self.data['Low']
        self.data['RSI'] = talib.RSI(close)
        self.data['MACD'], self.data['Signal'], _ = talib.MACD(close)
        self.data['OBV'] = talib.OBV(close, self.data['Volume'])
        self.data['SMA10'] = talib.SMA(close, timeperiod=10)
        self.data['SMA50'] = talib.SMA(close, timeperiod=50)

    def strategy_1(self):
        """
        BullishEngulfing + RSI(14) oversold + MACD histogram positive crossover
        """
        name = 'Strategy 1'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        bullish_engulfing = self.data['BullishEngulfing']
        oversold = self.data['RSI'] < 30
        macd_histogram = self.data['MACD'] - self.data['Signal'] > 0

        bullish_conditions = bullish_engulfing & oversold & macd_histogram

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[macd_histogram] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_2(self):
        """
        DarkCloudCover + RSI(14) overbought + OBV downtrend
        """
        name = 'Strategy 2'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        dark_cloud_cover = self.data['DarkCloudCover']
        overbought = self.data['RSI'] > 70
        obv_downtrend = self.data['OBV'].diff() < 0

        bearish_conditions =  dark_cloud_cover & overbought & obv_downtrend

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[overbought] += 1
        score[obv_downtrend] += 1

        self.scores[name] = score
        self.signals[name] = signals
    
    def strategy_3(self):
        """
        ThreeInsideUp + Close above SMA50 + OBV uptrend
        """
        name = 'Strategy 3'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_up = self.data['ThreeInsideUp']
        close_above_sma50 = self.data['Close'] > self.data['SMA50']
        obv_uptrend = self.data['OBV'].diff() > 0

        bullish_conditions = three_inside_up & close_above_sma50 & obv_uptrend

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[close_above_sma50] += 1
        score[obv_uptrend] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_4(self):
        """
        PiercingLine + RSI(14) oversold + Close above SMA50 + OBV uptrend
        """
        name = 'Strategy 4'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        piercing_line = self.data['PiercingLine']
        oversold = self.data['RSI'] < 30
        close_above_sma50 = self.data['Close'] > self.data['SMA50']
        obv_uptrend = self.data['OBV'].diff() > 0

        bullish_conditions = piercing_line & oversold & close_above_sma50 & obv_uptrend

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 4
        score[oversold] += 1
        score[close_above_sma50] += 1
        score[obv_uptrend] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_5(self):
        """
        BearishEngulfing + RSI(14) overbought + MACD histogram negative crossover
        """
        name = 'Strategy 5'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        bearish_engulfing = self.data['BearishEngulfing']
        overbought = self.data['RSI'] > 70
        macd_histogram = self.data['MACD'] - self.data['Signal'] < 0

        bearish_conditions = bearish_engulfing & overbought & macd_histogram

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[overbought] += 1
        score[macd_histogram] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_6(self):
        """
        ThreeInsideUp + RSI(14) oversold + OBV uptrend
        """
        name = 'Strategy 6'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_up = self.data['ThreeInsideUp']
        oversold = self.data['RSI'] < 30
        obv_uptrend = self.data['OBV'].diff() > 0

        bullish_conditions = three_inside_up & oversold & obv_uptrend

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[obv_uptrend] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_7(self):
        """
        Three Inside Up + RSI(14) oversold
        """
        name = 'Strategy 7'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_up = self.data['ThreeInsideUp']
        oversold = self.data['RSI'] < 30

        bullish_conditions = three_inside_up & oversold

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 2
        score[oversold] += 1

        self.scores[name] = score
        self.signals[name] = signals


    def strategy_8(self):
        """
        Three Inside Down + RSI(14) overbought
        """
        name = 'Strategy 8'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_down = self.data['ThreeInsideDown']
        overbought = self.data['RSI'] > 70

        bearish_conditions = three_inside_down & overbought

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 2
        score[overbought] += 1

        self.scores[name] = score
        self.signals[name] = signals


    def strategy_9(self):
        """
        Three White Soldiers + SMA10 > SMA50 + MACD bullish crossover
        """
        name = 'Strategy 9'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_white_soldiers = self.data['ThreeWhiteSoldiers']
        sma10_above_sma50 = self.data['SMA10'] > self.data['SMA50']
        macd_crossover = (self.data['MACD'] > self.data['Signal']) & (self.data['MACD'].shift(1) < self.data['Signal'].shift(1))

        bullish_conditions = three_white_soldiers & sma10_above_sma50 & macd_crossover

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[sma10_above_sma50] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals


    def strategy_10(self):
        """
        Three Black Crows + SMA10 < SMA50 + MACD bearish crossover
        """
        name = 'Strategy 10'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_black_crows = self.data['ThreeBlackCrows']
        sma10_below_sma50 = self.data['SMA10'] < self.data['SMA50']
        macd_crossover = (self.data['MACD'] < self.data['Signal']) & (self.data['MACD'].shift(1) > self.data['Signal'].shift(1))

        bearish_conditions = three_black_crows & sma10_below_sma50 & macd_crossover

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[sma10_below_sma50] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_11(self):
        """
        Three Inside Down + RSI(14) overbought + SMA10 < SMA50
        """
        name = 'Strategy 11'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_down = self.data['ThreeInsideDown']
        overbought = self.data['RSI'] > 70
        sma10_below_sma50 = self.data['SMA10'] < self.data['SMA50']

        bearish_conditions = three_inside_down & overbought & sma10_below_sma50

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[overbought] += 1
        score[sma10_below_sma50] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_12(self):
        """
        Three Inside Up + RSI(14) oversold + SMA10 > SMA50
        """
        name = 'Strategy 12'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_up = self.data['ThreeInsideUp']
        oversold = self.data['RSI'] < 30
        sma10_above_sma50 = self.data['SMA10'] > self.data['SMA50']

        bullish_conditions = three_inside_up & oversold & sma10_above_sma50

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[sma10_above_sma50] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_13(self):
        """
        Bullish Engulfing + OBV uptrend + RSI(14) oversold + SMA10 > SMA50
        """
        name = 'Strategy 13'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        bullish_engulfing = self.data['BullishEngulfing']
        obv_uptrend = self.data['OBV'].diff() > 0
        oversold = self.data['RSI'] < 30
        sma10_above_sma50 = self.data['SMA10'] > self.data['SMA50']

        bullish_conditions = bullish_engulfing & obv_uptrend & oversold & sma10_above_sma50

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 4
        score[obv_uptrend] += 1
        score[oversold] += 1
        score[sma10_above_sma50] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_14(self):
        """
        Three Inside Up + RSI(14) oversold + MACD bullish crossover
        """
        name = 'Strategy 14'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)
        three_inside_up = self.data['ThreeInsideUp']
        oversold = self.data['RSI'] < 30
        macd_crossover = (self.data['MACD'] > self.data['Signal']) & (self.data['MACD'].shift(1) < self.data['Signal'].shift(1))

        bullish_conditions = three_inside_up & oversold & macd_crossover

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_15(self):
        """
        Piercing Line + RSI(14) oversold + OBV uptrend
        """
        name = 'Strategy 15'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        piercing_line = self.data['PiercingLine']
        oversold = self.data['RSI'] < 30
        obv_uptrend = self.data['OBV'].diff() > 0

        bullish_conditions = piercing_line & oversold & obv_uptrend

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[obv_uptrend] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_16(self):
        """
        Evening Star + RSI(14) overbought + MACD bearish crossover
        """
        name = 'Strategy 16'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        evening_star = self.data['EveningStar']
        overbought = self.data['RSI'] > 70
        macd_crossover = (self.data['MACD'] < self.data['Signal']) & (self.data['MACD'].shift(1) > self.data['Signal'].shift(1))

        bearish_conditions = evening_star & overbought & macd_crossover

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[overbought] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_17(self):
        """
        Morning Star + RSI(14) oversold + OBV uptrend
        """
        name = 'Strategy 17'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        morning_star = self.data['MorningStar']
        oversold = self.data['RSI'] < 30
        obv_uptrend = self.data['OBV'].diff() > 0

        bullish_conditions = morning_star & oversold & obv_uptrend

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[oversold] += 1
        score[obv_uptrend] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_18(self):
        """
        Three Inside Down + SMA50 > SMA200 + RSI(14) overbought
        """
        name = 'Strategy 18'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        three_inside_down = self.data['ThreeInsideDown']
        sma50_above_sma200 = self.data['SMA50'] > self.data['SMA200']
        overbought = self.data['RSI'] > 70

        bearish_conditions = three_inside_down & sma50_above_sma200 & overbought

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[sma50_above_sma200] += 1
        score[overbought] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_19(self):
        """
        Morning Star + SMA10 > SMA50 + MACD bullish crossover
        """
        name = 'Strategy 19'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        morning_star = self.data['MorningStar']
        sma10_above_sma50 = self.data['SMA10'] > self.data['SMA50']
        macd_crossover = (self.data['MACD'] > self.data['Signal']) & (self.data['MACD'].shift(1) < self.data['Signal'].shift(1))

        bullish_conditions = morning_star & sma10_above_sma50 & macd_crossover

        signals[bullish_conditions] = 'buy'
        signals[~bullish_conditions] = 'hold'

        score[bullish_conditions] += 3
        score[sma10_above_sma50] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def strategy_20(self):
        """
        Evening Star + SMA10 < SMA50 + MACD bearish crossover
        """
        name = 'Strategy 20'
        score = np.zeros_like(self.data.index)
        signals = pd.Series(index=self.data.index, dtype=str)

        evening_star = self.data['EveningStar']
        sma10_below_sma50 = self.data['SMA10'] < self.data['SMA50']
        macd_crossover = (self.data['MACD'] < self.data['Signal']) & (self.data['MACD'].shift(1) > self.data['Signal'].shift(1))

        bearish_conditions = evening_star & sma10_below_sma50 & macd_crossover

        signals[bearish_conditions] = 'sell'
        signals[~bearish_conditions] = 'hold'

        score[bearish_conditions] += 3
        score[sma10_below_sma50] += 1
        score[macd_crossover] += 1

        self.scores[name] = score
        self.signals[name] = signals

    def get_top_strategy(self, n=1):
        """
        Get the top `n` strategies sorted by their scores.

        Args:
            n (int, optional): The number of top strategies to return. Defaults to 1.

        Returns:
            list: A list of top `n` strategy names.
        """
        # Calculate the total score for each strategy
        total_scores = self.scores.sum(axis=0)
        # Sort the strategies by their total scores
        sorted_strategies = total_scores.sort_values(ascending=False)
        # Return the top n strategies
        return sorted_strategies[:n].index.tolist()
