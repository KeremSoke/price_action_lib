#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  utils.py
#  
#  Copyright 2023 Kerem Soke <ksoke@student.ubc.ca>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import pandas as pd
import numpy as np
import requests
import io

def load_data(source, *args, **kwargs):
    """
    Load historical price data from a specified source (e.g., CSV file, API).

    Args:
        source (str): The data source identifier (e.g., file path, API endpoint).
        *args: Additional positional arguments required by the data source.
        **kwargs: Additional keyword arguments required by the data source.

    Returns:
        pd.DataFrame: A DataFrame containing the historical price data.
    """
    if source.lower().endswith('.csv'):
        return _load_data_from_csv(source, *args, **kwargs)
    elif source.lower().startswith('https://') or source.lower().startswith('http://'):
        return _load_data_from_api(source, *args, **kwargs)
    else:
        raise ValueError('Unsupported data source format.')

def _load_data_from_csv(file_path, **kwargs):
    """
    Load historical price data from a CSV file.

    Args:
        file_path (str): The file path of the CSV file.
        **kwargs: Additional keyword arguments to be passed to pd.read_csv().

    Returns:
        pd.DataFrame: A DataFrame containing the historical price data.
    """
    data = pd.read_csv(file_path, **kwargs)
    data['Date'] = pd.to_datetime(data['Date'])
    data.set_index('Date', inplace=True)
    return data

def _load_data_from_api(url, *args, **kwargs):
    """
    Load historical price data from an API.

    Args:
        url (str): The API endpoint URL.
        *args: Additional positional arguments required by the requests.get() function.
        **kwargs: Additional keyword arguments required by the requests.get() function.

    Returns:
        pd.DataFrame: A DataFrame containing the historical price data.
    """
    response = requests.get(url, *args, **kwargs)
    response.raise_for_status()
    data = pd.read_csv(io.StringIO(response.text))
    data['Date'] = pd.to_datetime(data['Date'])
    data.set_index('Date', inplace=True)
    return data

def preprocess_data(data, timeframe='1D'):
    """
    Preprocess the historical price data and resample it to the desired timeframe.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        timeframe (str): The desired timeframe for resampling (default is '1D').

    Returns:
        pd.DataFrame: A DataFrame containing the resampled price data.
    """
    resampled_data = data.resample(timeframe).agg({'Open': 'first', 'High': 'max', 'Low': 'min', 'Close': 'last', 'Volume': 'sum'})
    resampled_data.dropna(inplace=True)
    return resampled_data

def calculate_indicators(data, indicators=None):
    """
    Calculate various technical indicators for the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        indicators (list): A list of technical indicators to be calculated (default is None).

    Returns:
        pd.DataFrame: A DataFrame containing the price data with the calculated indicators.
    """
    if indicators is None:
        return data

    for indicator in indicators:
        if indicator == 'rolling_volatility':
            data['Rolling Volatility'] = calculate_rolling_volatility(data['Close'])
        elif indicator == 'rolling_mean':
            data['Rolling Mean'] = calculate_rolling_mean(data['Close'])
        elif indicator == 'rolling_std':
            data['Rolling Std'] = calculate_rolling_std(data['Close'])
        else:
            raise ValueError(f'Unsupported indicator: {indicator}')

    return data

def calculate_rolling_volatility(data, window=14):
    """
    Calculate the rolling volatility of the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The rolling window size for calculating volatility (default is 14).

    Returns:
        pd.Series: A Series containing the rolling volatility values.
    """
    log_returns = np.log(data / data.shift(1))
    rolling_volatility = log_returns.rolling(window=window).std() * np.sqrt(252)
    return rolling_volatility

def calculate_rolling_mean(data, window=14):
    """
    Calculate the rolling mean of the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The rolling window size for calculating the mean (default is 14).

    Returns:
        pd.Series: A Series containing the rolling mean values.
    """
    return data.rolling(window=window).mean()

def calculate_rolling_std(data, window=14):
    """
    Calculate the rolling standard deviation of the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The rolling window size for calculating the standard deviation (default is 14).

    Returns:
        pd.Series: A Series containing the rolling standard deviation values.
    """
    return data.rolling(window=window).std()

def calculate_percentage_change(data):
    """
    Calculate the percentage change between consecutive data points.

    Args:
        data (pd.Series): A Series containing the historical price data.

    Returns:
        pd.Series: A Series containing the percentage change values.
    """
    return data.pct_change()

def calculate_absolute_change(data):
    """
    Calculate the absolute change between consecutive data points.

    Args:
        data (pd.Series): A Series containing the historical price data.

    Returns:
        pd.Series: A Series containing the absolute change values.
    """
    return data.diff()

def normalize_data(data, method='min_max'):
    """
    Normalize the data using the specified normalization method.

    Args:
        data (pd.Series): A Series containing the historical price data.
        method (str): The normalization method to use ('min_max' or 'z_score').

    Returns:
        pd.Series: A Series containing the normalized data.
    """
    if method == 'min_max':
        return (data - data.min()) / (data.max() - data.min())
    elif method == 'z_score':
        return (data - data.mean()) / data.std()
    else:
        raise ValueError(f'Unsupported normalization method: {method}')


# Other utility functions or classes will be added here in the future.
