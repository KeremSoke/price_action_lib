#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  patterns.py
#  
#  Copyright 2023 Kerem Soke <ksoke@student.ubc.ca>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import pandas as pd
import numpy as np
from scipy.signal import argrelextrema
from scipy.stats import linregress
from sklearn.linear_model import LinearRegression

## TA

# Trends

def is_rising_trend(data, window=10, slope_threshold=0.02, consecutive_threshold=3):
    """
    Identify a rising trend in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to calculate the linear regression slope (default is 10).
        slope_threshold (float): The minimum threshold for the slope to be considered a rising trend (default is 0.02).
        consecutive_threshold (int): The minimum consecutive number of rising slopes to identify a rising trend (default is 3).

    Returns:
        pd.Series: A boolean Series indicating where the rising trend is identified.
    """
    # Initialize an empty Series to hold the rising trend
    rising_trend = pd.Series(False, index=data.index)

    # Check if there are enough data points
    if len(data) < window:
        return rising_trend

    # Loop through each data point starting from the window size
    for i in range(window, len(data)):
        # Select the data within the window
        window_data = data.iloc[i - window: i]

        # Calculate the linear regression slope using least squares method
        x = np.arange(window)
        y = window_data['Close'].values
        slope, _, _, _, _ = linregress(x, y)

        # Check if the slope is above the threshold
        if slope >= slope_threshold:
            # Check if there are enough consecutive rising slopes
            consecutive_rising_slopes = 1
            for j in range(i - window + 1, i):
                # Calculate the slope for each previous window
                prev_x = np.arange(window)
                prev_y = data.iloc[j - window: j]['Close'].values
                prev_slope, _, _, _, _ = linregress(prev_x, prev_y)

                # Check if the slope is above the threshold
                if prev_slope >= slope_threshold:
                    consecutive_rising_slopes += 1
                else:
                    break

            # Check if the consecutive rising slopes meet the threshold
            if consecutive_rising_slopes >= consecutive_threshold:
                rising_trend[data.index[i]] = True

    return rising_trend

def is_falling_trend(data, window=10, slope_threshold=-0.02, consecutive_threshold=3):
    """
    Identify a falling trend in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to calculate the linear regression slope (default is 10).
        slope_threshold (float): The maximum threshold for the slope to be considered a falling trend (default is -0.02).
        consecutive_threshold (int): The minimum consecutive number of falling slopes to identify a falling trend (default is 3).

    Returns:
        pd.Series: A boolean Series indicating where the falling trend is identified.
    """
    # Initialize an empty Series to hold the falling trend
    falling_trend = pd.Series(False, index=data.index)

    # Check if there are enough data points
    if len(data) < window:
        return falling_trend

    # Loop through each data point starting from the window size
    for i in range(window, len(data)):
        # Select the data within the window
        window_data = data.iloc[i - window: i]

        # Calculate the linear regression slope using least squares method
        x = np.arange(window)
        y = window_data['Close'].values
        slope, _, _, _, _ = linregress(x, y)

        # Check if the slope is below the threshold
        if slope <= slope_threshold:
            # Check if there are enough consecutive falling slopes
            consecutive_falling_slopes = 1
            for j in range(i - window + 1, i):
                # Calculate the slope for each previous window
                prev_x = np.arange(window)
                prev_y = data.iloc[j - window: j]['Close'].values
                prev_slope, _, _, _, _ = linregress(prev_x, prev_y)

                # Check if the slope is below the threshold
                if prev_slope <= slope_threshold:
                    consecutive_falling_slopes += 1
                else:
                    break

            # Check if the consecutive falling slopes meet the threshold
            if consecutive_falling_slopes >= consecutive_threshold:
                falling_trend[data.index[i]] = True

    return falling_trend

def is_range(data, window=20, range_threshold=0.05, consecutive_threshold=3):
    """
    Identify a range-bound market in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to calculate the range (default is 20).
        range_threshold (float): The maximum threshold for the range to be considered a range-bound market (default is 0.05).
        consecutive_threshold (int): The minimum consecutive number of range-bound periods to identify a range-bound market (default is 3).

    Returns:
        pd.Series: A boolean Series indicating where the range-bound market is identified.
    """
    # Initialize an empty Series to hold the range-bound market
    range_market = pd.Series(False, index=data.index)

    # Check if there are enough data points
    if len(data) < window:
        return range_market

    # Loop through each data point starting from the window size
    for i in range(window, len(data)):
        # Select the data within the window
        window_data = data.iloc[i - window: i]

        # Calculate the range as the difference between the maximum and minimum values
        price_range = window_data['High'].max() - window_data['Low'].min()

        # Calculate the range as a percentage of the average price
        average_price = window_data[['Open', 'Close']].mean().mean()
        range_percentage = price_range / average_price

        # Check if the range falls below the threshold
        if range_percentage <= range_threshold:
            # Check if there are enough consecutive range-bound periods
            consecutive_range_periods = 1
            for j in range(i - window + 1, i):
                # Calculate the range for each previous window
                prev_window_data = data.iloc[j - window: j]
                prev_price_range = prev_window_data['High'].max() - prev_window_data['Low'].min()
                prev_average_price = prev_window_data[['Open', 'Close']].mean().mean()
                prev_range_percentage = prev_price_range / prev_average_price

                # Check if the range falls below the threshold
                if prev_range_percentage <= range_threshold:
                    consecutive_range_periods += 1
                else:
                    break

            # Check if the consecutive range-bound periods meet the threshold
            if consecutive_range_periods >= consecutive_threshold:
                range_market[data.index[i]] = True

    return range_market

def is_rising_channel(data, window=20, channel_slope_threshold=0.01, channel_width_threshold=0.05, consecutive_threshold=3):
    """
    Identify a rising channel pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to calculate the channel (default is 20).
        channel_slope_threshold (float): The minimum threshold for the channel slope to be considered a rising channel (default is 0.01).
        channel_width_threshold (float): The maximum threshold for the channel width to be considered a rising channel (default is 0.05).
        consecutive_threshold (int): The minimum consecutive number of rising channel periods to identify a rising channel pattern (default is 3).

    Returns:
        pd.Series: A boolean Series indicating where the rising channel pattern is identified.
    """
    # Initialize an empty Series to hold the rising channel pattern
    rising_channel = pd.Series(False, index=data.index)

    # Check if there are enough data points
    if len(data) < window:
        return rising_channel

    # Loop through each data point starting from the window size
    for i in range(window, len(data)):
        # Select the data within the window
        window_data = data.iloc[i - window: i]

        # Fit a linear regression line to the high values
        x = np.arange(window).reshape((-1, 1))
        y = window_data['High'].values.reshape((-1, 1))
        model = LinearRegression()
        model.fit(x, y)
        channel_slope = model.coef_[0][0]

        # Calculate the width of the channel as the difference between the maximum and minimum low values
        channel_width = window_data['Low'].max() - window_data['Low'].min()

        # Check if the channel slope meets the threshold and the channel width is within the threshold
        if channel_slope >= channel_slope_threshold and channel_width <= channel_width_threshold:
            # Check if there are enough consecutive rising channel periods
            consecutive_rising_periods = 1
            for j in range(i - window + 1, i):
                # Fit a linear regression line to the high values for each previous window
                prev_window_data = data.iloc[j - window: j]
                x_prev = np.arange(window).reshape((-1, 1))
                y_prev = prev_window_data['High'].values.reshape((-1, 1))
                model_prev = LinearRegression()
                model_prev.fit(x_prev, y_prev)
                channel_slope_prev = model_prev.coef_[0][0]

                # Calculate the width of the channel for each previous window
                channel_width_prev = prev_window_data['Low'].max() - prev_window_data['Low'].min()

                # Check if the channel slope meets the threshold and the channel width is within the threshold
                if channel_slope_prev >= channel_slope_threshold and channel_width_prev <= channel_width_threshold:
                    consecutive_rising_periods += 1
                else:
                    break

            # Check if the consecutive rising channel periods meet the threshold
            if consecutive_rising_periods >= consecutive_threshold:
                rising_channel[data.index[i]] = True

    return rising_channel

def is_falling_channel(data, window=20, channel_slope_threshold=-0.01, channel_width_threshold=0.05, consecutive_threshold=3):
    """
    Identify a falling channel pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to calculate the channel (default is 20).
        channel_slope_threshold (float): The maximum threshold for the channel slope to be considered a falling channel (default is -0.01).
        channel_width_threshold (float): The maximum threshold for the channel width to be considered a falling channel (default is 0.05).
        consecutive_threshold (int): The minimum consecutive number of falling channel periods to identify a falling channel pattern (default is 3).

    Returns:
        pd.Series: A boolean Series indicating where the falling channel pattern is identified.
    """
    # Initialize an empty Series to hold the falling channel pattern
    falling_channel = pd.Series(False, index=data.index)

    # Check if there are enough data points
    if len(data) < window:
        return falling_channel

    # Loop through each data point starting from the window size
    for i in range(window, len(data)):
        # Select the data within the window
        window_data = data.iloc[i - window: i]

        # Fit a linear regression line to the low values
        x = np.arange(window).reshape((-1, 1))
        y = window_data['Low'].values.reshape((-1, 1))
        model = LinearRegression()
        model.fit(x, y)
        channel_slope = model.coef_[0][0]

        # Calculate the width of the channel as the difference between the maximum and minimum high values
        channel_width = window_data['High'].max() - window_data['High'].min()

        # Check if the channel slope meets the threshold and the channel width is within the threshold
        if channel_slope <= channel_slope_threshold and channel_width <= channel_width_threshold:
            # Check if there are enough consecutive falling channel periods
            consecutive_falling_periods = 1
            for j in range(i - window + 1, i):
                # Fit a linear regression line to the low values for each previous window
                prev_window_data = data.iloc[j - window: j]
                x_prev = np.arange(window).reshape((-1, 1))
                y_prev = prev_window_data['Low'].values.reshape((-1, 1))
                model_prev = LinearRegression()
                model_prev.fit(x_prev, y_prev)
                channel_slope_prev = model_prev.coef_[0][0]

                # Calculate the width of the channel for each previous window
                channel_width_prev = prev_window_data['High'].max() - prev_window_data['High'].min()

                # Check if the channel slope meets the threshold and the channel width is within the threshold
                if channel_slope_prev <= channel_slope_threshold and channel_width_prev <= channel_width_threshold:
                    consecutive_falling_periods += 1
                else:
                    break

            # Check if the consecutive falling channel periods meet the threshold
            if consecutive_falling_periods >= consecutive_threshold:
                falling_channel[data.index[i]] = True

    return falling_channel

# Formations

def is_head_and_shoulders(data, window=20, shoulder_height_tolerance=0.05, neck_line_tolerance=0.03):
    """
    Identify the Head and Shoulders pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima and minima.
        shoulder_height_tolerance (float): A tolerance factor to allow the shoulder heights to be within a certain range.
        neck_line_tolerance (float): A tolerance factor to allow the neckline to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Head and Shoulders pattern is identified.
    """
    # Calculate local maxima
    local_max = argrelextrema(data['Close'].values, np.greater, order=window)

    # Filter out maxima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])
    
    # Identify potential heads
    head_indices = maxima[(data['Close'][maxima] > data['Close'][maxima - window]) & 
                          (data['Close'][maxima] > data['Close'][maxima + window])]

    # Create a DataFrame of potential head values
    head_data = data.iloc[head_indices]
    
    # Initialize an empty Series to hold the Head and Shoulders pattern
    head_and_shoulders = pd.Series(False, index=data.index)

    # Loop through each head to see if a valid pattern exists
    for i in range(len(head_data)):
        head_index = head_data.index[i]
        head_value = head_data.iloc[i]['Close']
        
        # Look for a valid right shoulder
        right_shoulder_data = data.loc[head_index:].iloc[window:2*window]
        right_shoulder_min = right_shoulder_data['Close'].min()
        
        if abs(right_shoulder_min - head_value) / head_value < shoulder_height_tolerance:
            # Look for a valid left shoulder
            left_shoulder_data = data.loc[:head_index].iloc[-2*window:-window]
            left_shoulder_min = left_shoulder_data['Close'].min()
            
            if abs(left_shoulder_min - head_value) / head_value < shoulder_height_tolerance:
                # Check that the neckline is within the tolerance
                neck_line = (right_shoulder_min + left_shoulder_min) / 2
                if abs(neck_line - right_shoulder_min) / right_shoulder_min < neck_line_tolerance and \
                   abs(neck_line - left_shoulder_min) / left_shoulder_min < neck_line_tolerance:
                    # A valid Head and Shoulders pattern has been found
                    head_and_shoulders[head_index] = True
    
    return head_and_shoulders

def is_inverse_head_and_shoulders(data, window=20, shoulder_height_tolerance=0.05, neck_line_tolerance=0.03):
    """
    Identify the Inverse Head and Shoulders pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        shoulder_height_tolerance (float): A tolerance factor to allow the shoulder heights to be within a certain range.
        neck_line_tolerance (float): A tolerance factor to allow the neckline to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Inverse Head and Shoulders pattern is identified.
    """
    # Calculate local minima
    local_min = argrelextrema(data['Close'].values, np.less, order=window)

    # Filter out minima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Identify potential heads
    head_indices = minima[(data['Close'][minima] < data['Close'][minima - window]) &
                          (data['Close'][minima] < data['Close'][minima + window])]

    # Create a DataFrame of potential head values
    head_data = data.iloc[head_indices]

    # Initialize an empty Series to hold the Inverse Head and Shoulders pattern
    inverse_head_and_shoulders = pd.Series(False, index=data.index)

    # Loop through each head to see if a valid pattern exists
    for i in range(len(head_data)):
        head_index = head_data.index[i]
        head_value = head_data.iloc[i]['Close']

        # Look for a valid right shoulder
        right_shoulder_data = data.loc[head_index:].iloc[window:2 * window]
        right_shoulder_max = right_shoulder_data['Close'].max()

        if abs(right_shoulder_max - head_value) / head_value < shoulder_height_tolerance:
            # Look for a valid left shoulder
            left_shoulder_data = data.loc[:head_index].iloc[-2 * window:-window]
            left_shoulder_max = left_shoulder_data['Close'].max()

            if abs(left_shoulder_max - head_value) / head_value < shoulder_height_tolerance:
                # Check that the neckline is within the tolerance
                neck_line = (right_shoulder_max + left_shoulder_max) / 2
                if abs(neck_line - right_shoulder_max) / right_shoulder_max < neck_line_tolerance and \
                        abs(neck_line - left_shoulder_max) / left_shoulder_max < neck_line_tolerance:
                    # A valid Inverse Head and Shoulders pattern has been found
                    inverse_head_and_shoulders[head_index] = True

    return inverse_head_and_shoulders

def is_quasimodo(data, window=5, shoulder_height_tolerance=0.05, neck_line_tolerance=0.03):
    """
    Identify the Quasimodo pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima and minima.
        shoulder_height_tolerance (float): A tolerance factor to allow the shoulder heights to be within a certain range.
        neck_line_tolerance (float): A tolerance factor to allow the neckline to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Quasimodo pattern is identified.
    """
    # Calculate local maxima and minima
    local_max = argrelextrema(data['High'].values, np.greater, order=window)
    local_min = argrelextrema(data['Low'].values, np.less, order=window)

    # Filter out maxima and minima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Identify potential shoulders
    shoulder_indices = np.concatenate([maxima, minima])
    shoulder_indices.sort()

    # Create a DataFrame of potential shoulder values
    shoulder_data = data.iloc[shoulder_indices]

    # Initialize an empty Series to hold the Quasimodo pattern
    quasimodo = pd.Series(False, index=data.index)

    # Loop through each shoulder to see if a valid pattern exists
    for i in range(len(shoulder_data)):
        shoulder_index = shoulder_data.index[i]
        shoulder_value = shoulder_data.iloc[i]['High'] if shoulder_index in maxima else shoulder_data.iloc[i]['Low']

        # Look for a valid opposing shoulder
        opposing_shoulder_data = data.loc[:shoulder_index].iloc[-window:]
        opposing_shoulder_values = opposing_shoulder_data['High'] if shoulder_index in minima else opposing_shoulder_data['Low']

        # Check if the opposing shoulder values are within the tolerance range
        if np.all(np.abs(opposing_shoulder_values - shoulder_value) / shoulder_value < shoulder_height_tolerance):
            # Check that the neckline is within the tolerance range
            neck_line = opposing_shoulder_values.mean()
            if np.all(np.abs(neck_line - opposing_shoulder_values) / opposing_shoulder_values < neck_line_tolerance):
                # A valid Quasimodo pattern has been found
                quasimodo[shoulder_index] = True

    return quasimodo

def is_inverse_quasimodo(data, window=5, shoulder_height_tolerance=0.05, neck_line_tolerance=0.03):
    """
    Identify the Inversed Quasimodo pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima and minima.
        shoulder_height_tolerance (float): A tolerance factor to allow the shoulder heights to be within a certain range.
        neck_line_tolerance (float): A tolerance factor to allow the neckline to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Inversed Quasimodo pattern is identified.
    """
    # Calculate local maxima and minima
    local_max = argrelextrema(data['Low'].values, np.greater, order=window)
    local_min = argrelextrema(data['High'].values, np.less, order=window)

    # Filter out maxima and minima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Identify potential shoulders
    shoulder_indices = np.concatenate([maxima, minima])
    shoulder_indices.sort()

    # Create a DataFrame of potential shoulder values
    shoulder_data = data.iloc[shoulder_indices]

    # Initialize an empty Series to hold the Inversed Quasimodo pattern
    inversed_quasimodo = pd.Series(False, index=data.index)

    # Loop through each shoulder to see if a valid pattern exists
    for i in range(len(shoulder_data)):
        shoulder_index = shoulder_data.index[i]
        shoulder_value = shoulder_data.iloc[i]['Low'] if shoulder_index in maxima else shoulder_data.iloc[i]['High']

        # Look for a valid opposing shoulder
        opposing_shoulder_data = data.loc[:shoulder_index].iloc[-window:]
        opposing_shoulder_values = opposing_shoulder_data['Low'] if shoulder_index in minima else opposing_shoulder_data['High']

        # Check if the opposing shoulder values are within the tolerance range
        if np.all(np.abs(opposing_shoulder_values - shoulder_value) / shoulder_value < shoulder_height_tolerance):
            # Check that the neckline is within the tolerance range
            neck_line = opposing_shoulder_values.mean()
            if np.all(np.abs(neck_line - opposing_shoulder_values) / opposing_shoulder_values < neck_line_tolerance):
                # A valid Inversed Quasimodo pattern has been found
                inversed_quasimodo[shoulder_index] = True

    return inversed_quasimodo

def is_double_top(data, window=20, peak_tolerance=0.02):
    """
    Identify the Double Top pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima.
        peak_tolerance (float): A tolerance factor to allow the peaks to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Double Top pattern is identified.
    """
    # Calculate local maxima
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out maxima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Identify potential first tops
    first_top_indices = maxima[(data['High'][maxima] > data['High'][maxima - window]) &
                               (data['High'][maxima] > data['High'][maxima + window])]

    # Create a DataFrame of potential first top values
    first_top_data = data.iloc[first_top_indices]

    # Initialize an empty Series to hold the Double Top pattern
    double_top = pd.Series(False, index=data.index)

    # Loop through each first top to see if a valid pattern exists
    for i in range(len(first_top_data)):
        first_top_index = first_top_data.index[i]
        first_top_value = first_top_data.iloc[i]['High']

        # Look for a potential second top
        second_top_data = data.loc[first_top_index:].iloc[window:2 * window]
        second_top_value = second_top_data['High'].max()

        # Check if the second top is within the tolerance range
        if abs(second_top_value - first_top_value) / first_top_value < peak_tolerance:
            # A valid Double Top pattern has been found
            double_top[second_top_data.index] = True

    return double_top

def is_double_bottom(data, window=20, trough_tolerance=0.02):
    """
    Identify the Double Bottom pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima.
        trough_tolerance (float): A tolerance factor to allow the troughs to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Double Bottom pattern is identified.
    """
    # Calculate local minima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)

    # Filter out minima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Identify potential first bottoms
    first_bottom_indices = minima[(data['Low'][minima] < data['Low'][minima - window]) &
                                  (data['Low'][minima] < data['Low'][minima + window])]

    # Create a DataFrame of potential first bottom values
    first_bottom_data = data.iloc[first_bottom_indices]

    # Initialize an empty Series to hold the Double Bottom pattern
    double_bottom = pd.Series(False, index=data.index)

    # Loop through each first bottom to see if a valid pattern exists
    for i in range(len(first_bottom_data)):
        first_bottom_index = first_bottom_data.index[i]
        first_bottom_value = first_bottom_data.iloc[i]['Low']

        # Look for a potential second bottom
        second_bottom_data = data.loc[first_bottom_index:].iloc[window:2 * window]
        second_bottom_value = second_bottom_data['Low'].min()

        # Check if the second bottom is within the tolerance range
        if abs(second_bottom_value - first_bottom_value) / first_bottom_value < trough_tolerance:
            # A valid Double Bottom pattern has been found
            double_bottom[second_bottom_data.index] = True

    return double_bottom

def is_triple_top(data, window=20, peak_tolerance=0.02):
    """
    Identify the Triple Top pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima.
        peak_tolerance (float): A tolerance factor to allow the peaks to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Triple Top pattern is identified.
    """
    # Calculate local maxima
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out maxima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Identify potential first tops
    first_top_indices = maxima[(data['High'][maxima] > data['High'][maxima - window]) &
                               (data['High'][maxima] > data['High'][maxima + window])]

    # Create a DataFrame of potential first top values
    first_top_data = data.iloc[first_top_indices]

    # Initialize an empty Series to hold the Triple Top pattern
    triple_top = pd.Series(False, index=data.index)

    # Loop through each first top to see if a valid pattern exists
    for i in range(len(first_top_data)):
        first_top_index = first_top_data.index[i]
        first_top_value = first_top_data.iloc[i]['High']

        # Look for potential second top
        second_top_data = data.loc[first_top_index:].iloc[window:2 * window]
        second_top_value = second_top_data['High'].max()

        # Check if the second top is within the tolerance range
        if abs(second_top_value - first_top_value) / first_top_value < peak_tolerance:
            # Look for potential third top
            third_top_data = data.loc[first_top_index:].iloc[2 * window:3 * window]
            third_top_value = third_top_data['High'].max()

            # Check if the third top is within the tolerance range
            if abs(third_top_value - first_top_value) / first_top_value < peak_tolerance:
                # A valid Triple Top pattern has been found
                triple_top[second_top_data.index] = True
                triple_top[third_top_data.index] = True

    return triple_top

def is_triple_bottom(data, window=20, trough_tolerance=0.02):
    """
    Identify the Triple Bottom pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima.
        trough_tolerance (float): A tolerance factor to allow the troughs to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Triple Bottom pattern is identified.
    """
    # Calculate local minima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)

    # Filter out minima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Identify potential first bottoms
    first_bottom_indices = minima[(data['Low'][minima] < data['Low'][minima - window]) &
                                  (data['Low'][minima] < data['Low'][minima + window])]

    # Create a DataFrame of potential first bottom values
    first_bottom_data = data.iloc[first_bottom_indices]

    # Initialize an empty Series to hold the Triple Bottom pattern
    triple_bottom = pd.Series(False, index=data.index)

    # Loop through each first bottom to see if a valid pattern exists
    for i in range(len(first_bottom_data)):
        first_bottom_index = first_bottom_data.index[i]
        first_bottom_value = first_bottom_data.iloc[i]['Low']

        # Look for potential second bottom
        second_bottom_data = data.loc[first_bottom_index:].iloc[window:2 * window]
        second_bottom_value = second_bottom_data['Low'].min()

        # Check if the second bottom is within the tolerance range
        if abs(second_bottom_value - first_bottom_value) / first_bottom_value < trough_tolerance:
            # Look for potential third bottom
            third_bottom_data = data.loc[first_bottom_index:].iloc[2 * window:3 * window]
            third_bottom_value = third_bottom_data['Low'].min()

            # Check if the third bottom is within the tolerance range
            if abs(third_bottom_value - first_bottom_value) / first_bottom_value < trough_tolerance:
                # A valid Triple Bottom pattern has been found
                triple_bottom[second_bottom_data.index] = True
                triple_bottom[third_bottom_data.index] = True

    return triple_bottom

def is_ascending_triangle(data, window=20, body_tolerance=0.02, slope_tolerance=0.02):
    """
    Identify the Ascending Triangle pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Ascending Triangle pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Ascending Triangle pattern
    ascending_triangle = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid ascending line connecting the troughs
        upper_line_data = peak_data[peak_data.index > trough_index]
        for j in range(len(upper_line_data)):
            upper_line_index = upper_line_data.index[j]
            upper_line_value = upper_line_data.iloc[j]['High']

            # Check if the upper line is ascending
            if upper_line_value > trough_value:
                # Check if the lower line connects at least two previous troughs
                lower_line_data = trough_data[(trough_data.index < upper_line_index) & (trough_data.index > trough_index)]
                if len(lower_line_data) > 1:
                    lower_line_values = lower_line_data['Low'].values

                    # Check if the body sizes and slopes of the lines are within the tolerances
                    if (abs((upper_line_value - lower_line_values) / lower_line_values) < body_tolerance).all() and \
                            (np.diff(lower_line_values) / lower_line_values[:-1] > -slope_tolerance).all():
                        # A valid Ascending Triangle pattern has been found
                        ascending_triangle[lower_line_data.index] = True
                        ascending_triangle[upper_line_index] = True

    return ascending_triangle

def is_descending_triangle(data, window=20, body_tolerance=0.02, slope_tolerance=0.02):
    """
    Identify the Descending Triangle pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Descending Triangle pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Descending Triangle pattern
    descending_triangle = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid descending line connecting the peaks
        lower_line_data = peak_data[peak_data.index > trough_index]
        for j in range(len(lower_line_data)):
            lower_line_index = lower_line_data.index[j]
            lower_line_value = lower_line_data.iloc[j]['High']

            # Check if the lower line is descending
            if lower_line_value < trough_value:
                # Check if the upper line connects at least two previous peaks
                upper_line_data = trough_data[(trough_data.index < lower_line_index) & (trough_data.index > trough_index)]
                if len(upper_line_data) > 1:
                    upper_line_values = upper_line_data['Low'].values

                    # Check if the body sizes and slopes of the lines are within the tolerances
                    if (abs((upper_line_values - lower_line_value) / lower_line_value) < body_tolerance).all() and \
                            (np.diff(upper_line_values) / upper_line_values[:-1] < slope_tolerance).all():
                        # A valid Descending Triangle pattern has been found
                        descending_triangle[upper_line_data.index] = True
                        descending_triangle[lower_line_index] = True

    return descending_triangle

def is_symmetrical_triangle(data, window=20, body_tolerance=0.02, slope_tolerance=0.02):
    """
    Identify the Symmetrical Triangle pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Symmetrical Triangle pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Symmetrical Triangle pattern
    symmetrical_triangle = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid line connecting the troughs and peaks
        upper_line_data = peak_data[peak_data.index > trough_index]
        for j in range(len(upper_line_data)):
            upper_line_index = upper_line_data.index[j]
            upper_line_value = upper_line_data.iloc[j]['High']

            # Check if the upper line is ascending
            if upper_line_value > trough_value:
                # Check if the lower line connects at least two previous troughs
                lower_line_data = trough_data[(trough_data.index < upper_line_index) & (trough_data.index > trough_index)]
                if len(lower_line_data) > 1:
                    lower_line_values = lower_line_data['Low'].values

                    # Check if the body sizes and slopes of the lines are within the tolerances
                    if (abs((upper_line_value - lower_line_values) / lower_line_values) < body_tolerance).all() and \
                            (np.diff(lower_line_values) / lower_line_values[:-1] > -slope_tolerance).all():
                        # A valid Symmetrical Triangle pattern has been found
                        symmetrical_triangle[lower_line_data.index] = True
                        symmetrical_triangle[upper_line_index] = True

    return symmetrical_triangle

def is_rising_flag(data, window=20, body_tolerance=0.02, slope_tolerance=0.02):
    """
    Identify the Rising Flag pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Rising Flag pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Rising Flag pattern
    rising_flag = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid descending line connecting the peaks
        lower_line_data = peak_data[peak_data.index > trough_index]
        for j in range(len(lower_line_data)):
            lower_line_index = lower_line_data.index[j]
            lower_line_value = lower_line_data.iloc[j]['High']

            # Check if the lower line is descending
            if lower_line_value < trough_value:
                # Check if the upper line connects at least two previous peaks
                upper_line_data = trough_data[(trough_data.index < lower_line_index) & (trough_data.index > trough_index)]
                if len(upper_line_data) > 1:
                    upper_line_values = upper_line_data['Low'].values

                    # Check if the body sizes and slopes of the lines are within the tolerances
                    if (abs((upper_line_values - lower_line_value) / lower_line_value) < body_tolerance).all() and \
                            (np.diff(upper_line_values) / upper_line_values[:-1] < slope_tolerance).all():
                        # A valid Rising Flag pattern has been found
                        rising_flag[upper_line_data.index] = True
                        rising_flag[lower_line_index] = True

    return rising_flag

def is_falling_flag(data, window=20, body_tolerance=0.02, slope_tolerance=0.02):
    """
    Identify the Falling Flag pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Falling Flag pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Falling Flag pattern
    falling_flag = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid ascending line connecting the troughs
        upper_line_data = peak_data[peak_data.index > trough_index]
        for j in range(len(upper_line_data)):
            upper_line_index = upper_line_data.index[j]
            upper_line_value = upper_line_data.iloc[j]['High']

            # Check if the upper line is ascending
            if upper_line_value > trough_value:
                # Check if the lower line connects at least two previous troughs
                lower_line_data = trough_data[(trough_data.index < upper_line_index) & (trough_data.index > trough_index)]
                if len(lower_line_data) > 1:
                    lower_line_values = lower_line_data['Low'].values

                    # Check if the body sizes and slopes of the lines are within the tolerances
                    if (abs((upper_line_value - lower_line_values) / lower_line_values) < body_tolerance).all() and \
                            (np.diff(lower_line_values) / lower_line_values[:-1] > -slope_tolerance).all():
                        # A valid Falling Flag pattern has been found
                        falling_flag[lower_line_data.index] = True
                        falling_flag[upper_line_index] = True

    return falling_flag

def is_rounding_top(data, window=20, body_tolerance=0.02):
    """
    Identify the Rounding Top pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local maxima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Rounding Top pattern is identified.
    """
    # Calculate local maxima
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out maxima that do not have at least two data points on each side
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create a DataFrame of potential peak values
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Rounding Top pattern
    rounding_top = pd.Series(False, index=data.index)

    # Loop through each potential peak to see if a valid pattern exists
    for i in range(len(peak_data)):
        peak_index = peak_data.index[i]
        peak_value = peak_data.iloc[i]['High']

        # Look for a valid round shape of decreasing peaks
        lower_peaks_data = peak_data.loc[peak_index:].iloc[1:]
        lower_peaks_values = lower_peaks_data['High'].values

        # Check if the peaks form a decreasing round shape
        if (abs((lower_peaks_values - peak_value) / peak_value) < body_tolerance).all():
            # A valid Rounding Top pattern has been found
            rounding_top[lower_peaks_data.index] = True

    return rounding_top

def is_rounding_bottom(data, window=20, body_tolerance=0.02):
    """
    Identify the Rounding Bottom pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima.
        body_tolerance (float): A tolerance factor to allow the body sizes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Rounding Bottom pattern is identified.
    """
    # Calculate local minima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)

    # Filter out minima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])

    # Create a DataFrame of potential trough values
    trough_data = data.iloc[minima]

    # Initialize an empty Series to hold the Rounding Bottom pattern
    rounding_bottom = pd.Series(False, index=data.index)

    # Loop through each potential trough to see if a valid pattern exists
    for i in range(len(trough_data)):
        trough_index = trough_data.index[i]
        trough_value = trough_data.iloc[i]['Low']

        # Look for a valid round shape of increasing troughs
        higher_troughs_data = trough_data.loc[trough_index:].iloc[1:]
        higher_troughs_values = higher_troughs_data['Low'].values

        # Check if the troughs form an increasing round shape
        if (abs((higher_troughs_values - trough_value) / trough_value) < body_tolerance).all():
            # A valid Rounding Bottom pattern has been found
            rounding_bottom[higher_troughs_data.index] = True

    return rounding_bottom

def is_rising_pennant(data, window=20, volume_decrease_factor=0.5, slope_tolerance=0.2):
    """
    Identify the Rising Pennant pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        volume_decrease_factor (float): A factor to determine the maximum decrease in volume during the pennant.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Rising Pennant pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Rising Pennant pattern
    rising_pennant = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(1, len(trough_data)):
        first_trough_index = trough_data.index[i-1]
        first_trough_value = trough_data.iloc[i-1]['Low']
        first_trough_volume = trough_data.iloc[i-1]['Volume']

        second_trough_index = trough_data.index[i]
        second_trough_value = trough_data.iloc[i]['Low']
        second_trough_volume = trough_data.iloc[i]['Volume']

        # Check if the troughs are rising and the volume is decreasing
        if second_trough_value > first_trough_value and second_trough_volume < first_trough_volume * volume_decrease_factor:
            # Look for a valid resistance trendline
            resistance_data = peak_data.loc[first_trough_index:second_trough_index]

            if len(resistance_data) > 1:
                resistance_values = resistance_data['High'].values
                resistance_slope = (resistance_values[-1] - resistance_values[0]) / (len(resistance_values) - 1)

                # Check if the slope of the resistance trendline is within the tolerance range
                if np.abs(resistance_slope) / np.abs(second_trough_value - first_trough_value) < slope_tolerance:
                    # A valid Rising Pennant pattern has been found
                    rising_pennant[second_trough_index] = True

    return rising_pennant

def is_falling_pennant(data, window=20, volume_decrease_factor=0.5, slope_tolerance=0.2):
    """
    Identify the Falling Pennant pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        volume_decrease_factor (float): A factor to determine the maximum decrease in volume during the pennant.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Falling Pennant pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Falling Pennant pattern
    falling_pennant = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(1, len(peak_data)):
        first_peak_index = peak_data.index[i-1]
        first_peak_value = peak_data.iloc[i-1]['High']
        first_peak_volume = peak_data.iloc[i-1]['Volume']

        second_peak_index = peak_data.index[i]
        second_peak_value = peak_data.iloc[i]['High']
        second_peak_volume = peak_data.iloc[i]['Volume']

        # Check if the peaks are falling and the volume is decreasing
        if second_peak_value < first_peak_value and second_peak_volume < first_peak_volume * volume_decrease_factor:
            # Look for a valid support trendline
            support_data = trough_data.loc[first_peak_index:second_peak_index]

            if len(support_data) > 1:
                support_values = support_data['Low'].values
                support_slope = (support_values[-1] - support_values[0]) / (len(support_values) - 1)

                # Check if the slope of the support trendline is within the tolerance range
                if np.abs(support_slope) / np.abs(first_peak_value - second_peak_value) < slope_tolerance:
                    # A valid Falling Pennant pattern has been found
                    falling_pennant[second_peak_index] = True

    return falling_pennant

def is_rising_wedge(data, window=20, slope_tolerance=0.2):
    """
    Identify the Rising Wedge pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Rising Wedge pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Rising Wedge pattern
    rising_wedge = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(1, len(trough_data)):
        first_trough_index = trough_data.index[i-1]
        first_trough_value = trough_data.iloc[i-1]['Low']

        second_trough_index = trough_data.index[i]
        second_trough_value = trough_data.iloc[i]['Low']

        # Check if the troughs are rising
        if second_trough_value > first_trough_value:
            # Look for a valid support trendline
            support_data = peak_data.loc[first_trough_index:second_trough_index]

            if len(support_data) > 1:
                support_values = support_data['Low'].values
                support_slope = (support_values[-1] - support_values[0]) / (len(support_values) - 1)

                # Check if the slope of the support trendline is within the tolerance range
                if np.abs(support_slope) / np.abs(second_trough_value - first_trough_value) < slope_tolerance:
                    # A valid Rising Wedge pattern has been found
                    rising_wedge[second_trough_index] = True

    return rising_wedge

def is_falling_wedge(data, window=20, slope_tolerance=0.2):
    """
    Identify the Falling Wedge pattern in the price data.

    Args:
        data (pd.DataFrame): A DataFrame containing the historical price data.
        window (int): The size of the window to identify local minima and maxima.
        slope_tolerance (float): A tolerance factor to allow the slopes to be within a certain range.

    Returns:
        pd.Series: A boolean Series indicating where the Falling Wedge pattern is identified.
    """
    # Calculate local minima and maxima
    local_min = argrelextrema(data['Low'].values, np.less, order=window)
    local_max = argrelextrema(data['High'].values, np.greater, order=window)

    # Filter out minima and maxima that do not have at least two data points on each side
    minima = np.array([i for i in local_min[0] if i > window and i < len(data) - window])
    maxima = np.array([i for i in local_max[0] if i > window and i < len(data) - window])

    # Create DataFrames of potential trough and peak values
    trough_data = data.iloc[minima]
    peak_data = data.iloc[maxima]

    # Initialize an empty Series to hold the Falling Wedge pattern
    falling_wedge = pd.Series(False, index=data.index)

    # Loop through each potential pattern to see if a valid pattern exists
    for i in range(1, len(trough_data)):
        first_trough_index = trough_data.index[i-1]
        first_trough_value = trough_data.iloc[i-1]['Low']

        second_trough_index = trough_data.index[i]
        second_trough_value = trough_data.iloc[i]['Low']

        # Check if the troughs are falling
        if second_trough_value < first_trough_value:
            # Look for a valid resistance trendline
            resistance_data = peak_data.loc[first_trough_index:second_trough_index]

            if len(resistance_data) > 1:
                resistance_values = resistance_data['High'].values
                resistance_slope = (resistance_values[-1] - resistance_values[0]) / (len(resistance_values) - 1)

                # Check if the slope of the resistance trendline is within the tolerance range
                if np.abs(resistance_slope) / np.abs(second_trough_value - first_trough_value) < slope_tolerance:
                    # A valid Falling Wedge pattern has been found
                    falling_wedge[second_trough_index] = True

    return falling_wedge

